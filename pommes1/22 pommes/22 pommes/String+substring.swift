//
//  String+substring.swift
//  22 pommes
//
//  Created by Richard Urunuela on 13/03/2017.
//  Copyright © 2017 Richard Urunuela. All rights reserved.
//

import Foundation
public extension String {
    
    func substring(_ r: Range<Int>) -> String {
        let fromIndex = self.index(self.startIndex, offsetBy: r.lowerBound)
        let toIndex = self.index(self.startIndex, offsetBy: r.upperBound)
        return self.substring(with: Range<String.Index>(uncheckedBounds: (lower: fromIndex, upper: toIndex)))
    }
    
}
