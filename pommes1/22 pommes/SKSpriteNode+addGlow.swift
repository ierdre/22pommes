//
//  SKSpriteNode+addGlow.swift
//  22 pommes
//
//  Created by Richard Urunuela on 10/05/2017.
//  Copyright © 2017 Richard Urunuela. All rights reserved.
//

import Foundation
import SpriteKit
extension SKSpriteNode {
    
    func addGlow(radius: Float = 30) {
        let effectNode = SKEffectNode()
        effectNode.shouldRasterize = true
        addChild(effectNode)
        effectNode.addChild(SKSpriteNode(texture: texture))
        effectNode.filter = CIFilter(name: "CIGaussianBlur", withInputParameters: ["inputRadius":radius])
    }
}
