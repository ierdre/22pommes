//
//  gameIA.swift
//  22 pommes
//
//  Created by Richard Urunuela on 13/03/2017.
//  Copyright © 2017 Richard Urunuela. All rights reserved.
//

import Foundation
func recMinMAx(verger: [[Int]],v1:Int,r1:Int,v2:Int,r2:Int,xc:Int,yc:Int,tour:Bool,profondeur:Int)->Int{
  
    var res = 0
    var nr1 = r1
    var nr2 = r2
    var nv1 = v1
    var nv2 = v2
    
    if (v1 == 11) && (r1 == 11) {return 1}
    if (v2 == 11) && (r2 == 11) {return -1}
    if v1 >= 11 {return -1}
    if r1 >= 11 {return -1}
    if v2 > 11 {return 1}
    if r2 > 11 {return 1}
    if (profondeur == 0 ) {return res}
    
    //Exploration
    for x in 0...4 {
        if (verger[x][yc] != 0 ){
            let fruitc = verger[x][yc]
            if fruitc < 0 {
                if tour {
                    nr1 = r1 - fruitc
                }
                else {
                    nr2 = r2 - fruitc
                }
            }
            else {
                if tour {
                    nv1 = v1 + fruitc
                }
                else {
                    nv2 = v2 + fruitc
                }
            }
            
            var rverger = verger
            rverger[x][yc] = 0
            res = res + recMinMAx(verger: rverger, v1: nv1, r1: nr1, v2: nv2, r2: nr2, xc: x, yc: yc,tour: !tour,  profondeur: profondeur - 1)
        }
        
        
        
    }
    for y in 0...4 {
        if (verger[xc][y] != 0 ){
            let fruitc = verger[xc][y]
            if fruitc < 0 {
                if tour {
                    nr1 = r1 - fruitc
                }
                else {
                    nr2 = r2 - fruitc
                }
            }
            else {
                if tour {
                    nv1 = v1 + fruitc
                }
                else {
                    nv2 = v2 + fruitc
                }
            }
            
            var rverger = verger
            rverger[xc][y] = 0
            res = res + recMinMAx(verger: rverger, v1: nv1, r1: nr1, v2: nv2, r2: nr2, xc: xc, yc: y,tour: !tour,  profondeur: profondeur - 1)
        }
        
        
        
    }
    
    
    
    return res
    
}

func affRecMac (verger: [[Int]],v1:Int,r1:Int,v2:Int,r2:Int,xc:Int,yc:Int,tour:Bool,profondeur:Int)->(Int,Int){
    var line = 0
    var col = 0
    var max = -100000
    for x in 0...4 {
        
        if (verger[x][yc] != 0) {
            let ve2 = verger
            var res = recMinMAx(verger: ve2 , v1: v1, r1: r1, v2: v2, r2: r2, xc: x, yc: yc,tour: tour,  profondeur: profondeur)
            debugPrint("\(x) \(yc) = \(res )")
            
            if (res > max){
                max = res
                col = yc
                line = x
            }
            
        }
        
    }
    for y in 0...4{
        if (verger[xc][y] != 0) {
            let ve2 = verger
            var res = recMinMAx(verger: ve2 , v1: v1, r1: r1, v2: v2, r2: r2, xc: xc, yc: yc,tour: tour,  profondeur: profondeur)
            debugPrint("\(xc) \(y) = \(res )")
            if (res > max){
                max = res
                col = y
                line = xc
            }
        }
    }
    return (line,col)
    
    
    
}
