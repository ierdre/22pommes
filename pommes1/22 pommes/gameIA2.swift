//
//  gameIA2.swift
//  22 pommes
//
//  Created by Richard Urunuela on 14/03/2017.
//  Copyright © 2017 Richard Urunuela. All rights reserved.
//

import Foundation

func recMinMax2(verger :[[Int]],
               pommesVerte1:Int,
               pommesRouge1:Int,
               pommesVerte2:Int,
               pommesRouge2:Int,
               tour:Bool,
               ceilleurColonne:Int,
               ceilleurLigne:Int,
               profondeur:Int,delta:Int)->Int{
    var res = 0
    
    var pv1 = pommesVerte1
    var pv2 = pommesVerte2
    var pr1 = pommesRouge1
    var pr2 = pommesRouge2
    
    
    if (pv1 == 11) && (pr1 == 11 ) {return Int((1.0/Double(delta))*100)}
    if (pv2 == 11) && (pr2 == 11 ) {return -Int((1.0/Double(delta))*100)}
    if pv1 > 11 {return -Int((1.0/Double(delta))*100)}
    if pr1 > 11 {return -Int((1.0/Double(delta))*100)}
    if pv2 > 11 {return Int((1.0/Double(delta))*100)}
    if pr2 > 11 {return Int((1.0/Double(delta))*100)}
    
    if (profondeur == 0 ) {return (res)}
    
    for colonne in 0...4{
        let val = verger[ceilleurLigne][colonne]
        if (val != 0 ) {
            var tmpverger = verger
            tmpverger[ceilleurLigne][colonne] = 0
            if val > 0 {
                if !tour  {pv1 = pommesVerte1 + val }
                else {pv2 = pommesVerte2 + val}
            }
            else {
                if !tour { pr1 = pommesRouge1 - val }
                else {pr2 = pommesRouge2 - val}
            }
            
            //Enf val !=0
            res = res + recMinMax2(verger: tmpverger  ,
                                  pommesVerte1: pv1,
                                  pommesRouge1: pr1,
                                  pommesVerte2: pv2,
                                  pommesRouge2: pr2,
                                  tour: !tour,
                                  ceilleurColonne: colonne,
                                  ceilleurLigne: ceilleurLigne,
                                  profondeur: profondeur-1,delta:abs(val))
        }
        
    }
    
    for ligne in 0...4{
        let val = verger[ligne][ceilleurColonne]
        if (val != 0 ) {
            var tmpverger = verger
            tmpverger[ligne][ceilleurColonne] = 0
            if val > 0 {
                if !tour  {pv1 = pommesVerte1 + val }
                else {pv2 = pommesVerte2 + val}
            }
            else {
                if !tour { pr1 = pommesRouge1 - val }
                else {pr2 = pommesRouge2 - val}
            }
            
            //Enf val !=0
            res = res + recMinMax2(verger: tmpverger  ,
                                  pommesVerte1: pv1,
                                  pommesRouge1: pr1,
                                  pommesVerte2: pv2,
                                  pommesRouge2: pr2,
                                  tour: !tour,
                                  ceilleurColonne: ceilleurColonne,
                                  ceilleurLigne: ligne,
                                  profondeur: profondeur-1,delta:abs(val))
        }
        
    }
    
    
    
    
    return (res)
    
}
func getGoodchoice(verger :[[Int]],
                   pommesVerte1:Int,
                   pommesRouge1:Int,
                   pommesVerte2:Int,
                   pommesRouge2:Int,
                   
                   ceilleurColonne:Int,
                   ceilleurLigne:Int,
                   profondeur:Int)->(Int,Int){
    var line = 0
    var col = 0
    var max = -10000000
    
    for colonne in 0...4{
        
        let val = verger[ceilleurLigne][colonne]
        if val != 0 { //Il ya des pommes
            var tmpVerger  = verger
            tmpVerger[ceilleurLigne][colonne] = 0
            if (val>0){
                if (pommesVerte2+val) <= 11 {
                let res = recMinMax2(verger: tmpVerger  ,
                                    pommesVerte1: pommesVerte1,
                                    pommesRouge1: pommesRouge1,
                                    pommesVerte2: pommesVerte2+val,
                                    pommesRouge2: pommesRouge2,
                                    tour: false,
                                    ceilleurColonne: colonne,
                                    ceilleurLigne: ceilleurLigne,
                                    profondeur: profondeur,delta:abs(val))
                debugPrint("1  \(pommesVerte1 ) \(ceilleurLigne) \(colonne) \(res) \(val)")
                if (res > max){
                    max = res
                    col = colonne
                    line = ceilleurLigne
                }
                }
            }
            if (val<0){
                if (pommesRouge2-val) <= 11 {
                let res = recMinMax2(verger: tmpVerger  ,
                                    pommesVerte1: pommesVerte1,
                                    pommesRouge1: pommesRouge1,
                                    pommesVerte2: pommesVerte2,
                                    pommesRouge2: pommesRouge2-val,
                                    tour: false,
                                    ceilleurColonne: colonne,
                                    ceilleurLigne: ceilleurLigne,
                                    profondeur: profondeur,delta:abs(val))
                    debugPrint("2  \(pommesRouge1 ) \(ceilleurLigne) \(colonne) \(res) \(-val)")
                if (res > max){
                    max = res
                    col = colonne
                    line = ceilleurLigne
                }
                }
            }
            
            
        }
        
    }
    for ligne in 0...4{
        
        let val = verger[ligne][ceilleurColonne]
        if val != 0 { //Il ya des pommes
            var tmpVerger  = verger
            tmpVerger[ligne][ceilleurColonne] = 0
            if (val>0){
                 if (pommesVerte2+val) <= 11 {
                let res = recMinMax2(verger: tmpVerger  ,
                                    pommesVerte1: pommesVerte1,
                                    pommesRouge1: pommesRouge1,
                                    pommesVerte2: pommesVerte2+val,
                                    pommesRouge2: pommesRouge2,
                                    tour: false,
                                    ceilleurColonne: ceilleurColonne,
                                    ceilleurLigne: ligne,
                                    profondeur: profondeur,delta:abs(val))
                debugPrint("3  \(pommesVerte1 ) \(ligne) \(ceilleurColonne) \(res) \(val)")
                if (res > max){
                    max = res
                    col = ceilleurColonne
                    line = ligne
                }
                }
            }
            if (val<0){
                if (pommesRouge2-val) <= 11 {
                let res = recMinMax2(verger: tmpVerger  ,
                                    pommesVerte1: pommesVerte1,
                                    pommesRouge1: pommesRouge1,
                                    pommesVerte2: pommesVerte2,
                                    pommesRouge2: pommesRouge2-val,
                                    tour: false,
                                    ceilleurColonne: ceilleurColonne,
                                    ceilleurLigne: ligne,
                                    profondeur: profondeur,delta:abs(val))
                    debugPrint("4 \(pommesRouge1 ) \(ligne) \(ceilleurColonne) \(res) \(-val)")
                if (res > max){
                    max = res
                    col = ceilleurColonne
                    line = ligne
                }
                }
            }
            
            
        }
        
    }
    
      return (line,col)
    
    
}
