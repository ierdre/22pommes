//
//  gameIA3-oc.swift
//  22 pommes
//
//  Created by Richard Urunuela on 14/04/2017.
//  Copyright © 2017 Richard Urunuela. All rights reserved.
//

import Foundation



//: Playground - noun: a place where people can play


/*
 Joueur 1 joueur humain
 Joueur 2 joueur oridnateur
 */
var str = "Hello, playground"
enum SystemError: Error {
    case invalidCase
}
enum Joueur: String {
    case humain
    case ordinateur
}
var originalArray = [[-1, 3, 5, 1,-3],
                     [-5,-5,2,1,5],
                     [5,1,2,-1,3],
                     [1,0,-3,-5,-5],
                     [-3,5,3,-2,-1]
]


struct  plateauStruct{
    var verger =  [[0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0]]
    var pommeRouge1 = 0
    var pommeVerte1 = 0
    var pommeRouge2 = 0
    var pommeVerte2 = 0
    var profondeur = 0
    var colonneC =  0
    var ligneC = 0
    var nombreCoup = 0
    var level =  3
    
    
}

typealias mCoup = ( colonne:Int ,ligne:Int)
func joueurprendre(plateau : inout plateauStruct,ligne : Int , colonne : Int) {
    var val = plateau.verger[ligne][colonne ]
    if val < 0 {
        plateau.pommeRouge1 = plateau.pommeRouge1 -  val
    }// pommme rouge
    else {
        plateau.pommeVerte1 = plateau.pommeVerte1  + val
    }
    plateau.verger[ligne][colonne ] = 0
    plateau.colonneC = colonne
    plateau.ligneC = ligne
    
}

func ordiprendre(plateau : inout plateauStruct,ligne : Int , colonne : Int) throws{
    var val = plateau.verger[ligne][colonne ]
    if (val == 0  ) {
        throw SystemError.invalidCase
    }
    if val < 0 {
        plateau.pommeRouge2 = plateau.pommeRouge2 -  val
    }// pommme rouge
    else {
        plateau.pommeVerte2 = plateau.pommeVerte2  + val
    }
    plateau.verger[ligne][colonne ] = 0
    plateau.colonneC = colonne
    plateau.ligneC = ligne
    
    
}
func inCnbreCoup(plateau:inout plateauStruct){
    plateau.nombreCoup = plateau.nombreCoup + 1
}
func evalJeux (plateau : plateauStruct, joueur: Joueur)->Int{
    
    //LEVEL 3
    if (plateau.level > 2 ){
    if (plateau.pommeRouge1 == 11) && (plateau.pommeVerte1 == 11) {return -100+plateau.nombreCoup}
    if (plateau.pommeRouge2 == 11) && (plateau.pommeVerte2 == 11) {return 100-plateau.nombreCoup}
    }
    //LEVEL 2
     if (plateau.level > 1 ){
        if plateau.pommeRouge1 > 11 {return 50-plateau.nombreCoup}
        if plateau.pommeVerte1 > 11 {return 50-plateau.nombreCoup}
    }
    //LEVEL 1
     if (plateau.level > 0 ){
        if plateau.pommeRouge2 > 11 {return -50+plateau.nombreCoup}
        if plateau.pommeVerte2 > 11 {return -50+plateau.nombreCoup}
    }
    //LEVEL 0
    
    return 0
}
func Max (plateau : plateauStruct,profondeur : Int)->Int{
    if profondeur == 0 {
        return Int.min
        
    }
    let eval = evalJeux(plateau : plateau,joueur:Joueur.ordinateur)
    if  eval != 0 {
        return eval
    }
    var max_val = Int.min
    
    
    for place in 0...9{
        
        let colonne = place > 4 ? place - 5 : plateau.colonneC
        let ligne   = place > 4 ? plateau.ligneC : place
        var plateauCopy = plateau
        var val = plateauCopy.verger[ligne][colonne]
        if (val != 0){
            
            do {
                try ordiprendre(plateau: &plateauCopy,ligne: ligne,colonne: colonne)
                inCnbreCoup(plateau:&plateauCopy)
                //Simulation
                let valMin = Min(plateau: plateauCopy,profondeur:profondeur-1)
                if valMin > max_val {
                    max_val = valMin
                }
            }
            catch{
                debugPrint(" ERREUR ")
            }
            
        }
        
        
    }
    
    
    return max_val
    
}
func Min (plateau : plateauStruct,profondeur : Int)->Int{
    if profondeur == 0 {
        return Int.max
        
    }
    let eval = evalJeux(plateau : plateau,joueur:Joueur.humain)
    if  eval != 0 {
        return eval
    }
    var  min_val = Int.max
    
    for place in 0...9{
        
        let colonne = place > 4 ? place - 5 : plateau.colonneC
        let ligne   = place > 4 ? plateau.ligneC : place
        var plateauCopy = plateau
        var val = plateauCopy.verger[ligne][colonne]
        if (val != 0){
            joueurprendre(plateau: &plateauCopy,ligne: ligne,colonne: colonne)
            inCnbreCoup(plateau:&plateauCopy)
            
            let valMin =  Max (plateau: plateauCopy, profondeur:profondeur-1)
            if valMin < min_val {
                min_val = valMin
            }
            
        }
    }
    
    return min_val
}

/*//func getGoodchoice(verger :[[Int]],
           //        pommesVerte1:Int,
            //       pommesRouge1:Int,
            //       pommesVerte2:Int,
                   pommesRouge2:Int,
                   
                   ceilleurColonne:Int,
                   ceilleurLigne:Int,
                   profondeur:Int)->(Int,Int){
*/
func getGoodchoice3 (verger: [[Int]],
                     pommeVerte1: Int,
                     pommeRouge1 : Int,
                     pommeVerte2: Int,
                     pommeRouge2:Int,
                     colonneC: Int,
                     ligneC: Int,
                     profondeur:Int,
                     level:Int) ->(Int,Int)
/*func jouer (verger: [[Int]], colonneC: Int, ligneC: Int,tour:Bool,profondeur:Int,
            pommeRouge1: Int,
            pommeVerte1 : Int,
            pommeRouge2: Int,
            pommeVerte2:Int)*/
{
    
    let plateauInit = plateauStruct(verger : verger,
                              pommeRouge1 : pommeRouge1,
                              pommeVerte1 : pommeVerte1,
                              pommeRouge2 : pommeRouge2,
                              pommeVerte2 : pommeVerte2,
                              profondeur  :0,
                              colonneC : colonneC,
                              ligneC : ligneC,
                              nombreCoup : 0,
                              level : level)
    
    var  meilleurCoup : mCoup
    meilleurCoup.colonne  = -1
    meilleurCoup.ligne = -1
    var max_val = Int.min
    for place in 0...9{
        
        
        let colonne = place > 4 ? place - 5 : colonneC
        let ligne   = place > 4 ? ligneC : place
        var plateauCopy = plateauInit
        var val = plateauCopy.verger[ligne][colonne]
        //debugPrint(" LEvel \(plateauCopy.level)")
        if (val != 0){
            
            if meilleurCoup.ligne == -1 {
                meilleurCoup.ligne = ligne
                meilleurCoup.colonne = colonne
            }
            do {
                try ordiprendre(plateau: &plateauCopy,ligne: ligne,colonne: colonne)
                //Simulation
               // debugPrint("\(ligne) \(colonne)")
                let valMin = Min(plateau: plateauCopy,profondeur:profondeur)
                debugPrint("\(ligne) \(colonne) \(valMin)")
                
                if valMin > max_val {
                    max_val = valMin
                    meilleurCoup.colonne  = colonne
                    meilleurCoup.ligne = ligne
                    
                }
                else if valMin ==  max_val{
                    
                    let oldVal = plateauCopy.verger[meilleurCoup.ligne][meilleurCoup.colonne]
                    debugPrint("SAME \(oldVal) \(val)" );
                    // Ici en cas de pb on choisit la plus petite valeur 
                    // Merci seb :-)
                    if abs(val) < abs(oldVal){
                        meilleurCoup.colonne  = colonne
                        meilleurCoup.ligne = ligne
                    }
                }
                
                
            }
            catch{
                debugPrint(" ERREUR ")
            }
            
        }
        
        
    }
    return(meilleurCoup.ligne,meilleurCoup.colonne)
   
    
}


    
    
   
    


