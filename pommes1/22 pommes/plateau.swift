//
//  plateau.swift
//  22 pommes
//
//  Created by Richard Urunuela on 12/03/2017.
//  Copyright © 2017 Richard Urunuela. All rights reserved.
//

import UIKit
import SpriteKit
class plateau: SKScene {
  
    
    //View
    
    var viewController:ViewController?
    
    var selectable = true
    //tabOrigine
    var pommiers = [[-1, 3, 5, 1,-3],
                         [-5,-5,2,1,5],
                         [5,1,2,-1,3],
                         [2,0,-3,-5,-5],
                         [-3,5,3,-2,-1]
    ]
    var oldPommiers = [[Int]]()
    //-5 -4 -3 -2 -1 0 1 2 3 4 5
    var iconePommier = ["pionrouge05","","pionrouge03","pionrouge02","pionrouge01","",
                        "pionvert01","pionvert02","pionvert03","","pionvert05"]
    
    //posinitial ceuilleur
    var line = 3
    var col = 1
    
    var pommeRouge1 = 0
    var pommeVerte1 = 0
    var pommeRouge2 = 0
    var pommeVerte2 = 0
    
    var tour = true
    var isEnable = true
    var refresh  = false
    var nbTour  =  0
    var level = 3
    
    func solVeFinJeu(){
        if (pommeRouge1 > 11) {
            afficherPerdue()
        }
        if (pommeVerte1 > 11){
            afficherPerdue()
        }
        if (pommeRouge2 > 11) {
            afficherGagne()
        }
        if (pommeVerte2 > 11 ){
            afficherGagne()
        }
        if (pommeRouge1 == 11) && (pommeVerte1 == 11){
            afficherGagne()

        }
        if (pommeRouge2 == 11) && (pommeVerte2 == 11){
           afficherPerdue()
            
        }
    }
    
    func selectLevel(){
        let alert = UIAlertController(title: "Niveau de difficulté ", message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "level 0", style: .default, handler: {[weak self] _ in self?.level = 0  } ))
        
        alert.addAction(UIAlertAction(title: "level 1", style: .default, handler: {[weak self] _ in self?.level = 1  } ))
        
        alert.addAction(UIAlertAction(title: "level 2", style: .default, handler: {[weak self] _ in self?.level = 2  } ))
        
        alert.addAction(UIAlertAction(title: "level 3", style: .default, handler: {[weak self] _ in self?.level = 3  } ))
        viewController?.present(alert, animated: false, completion:nil)
        
    }
    func  afficherGagne(){
        nbTour = 0
        let alert = UIAlertController(title: "Vous avez gagné , bravo ", message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Recommencer une nouvelle partie", style: .cancel, handler: {[weak self] _ in self?.refreshG() } ))
        
        viewController?.present(alert, animated: false, completion:nil)
    }
    func afficherPerdue(){
        nbTour = 0
        let alert = UIAlertController(title: "Vous avez perdu  :-( ", message: "", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Recommencer une nouvelle partie  ", style: .cancel, handler: {[weak self] _ in self?.refreshG() } ))
        
        viewController?.present(alert, animated: false, completion:nil)
    }
    func  refreshGame(){
        
        nbTour = 0
        
        let alert = UIAlertController(title: "Recommencer une nouvelle partie \(String(format: "%C", 0x2764))", message: "", preferredStyle: .alert)
        
     //   alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {[weak self] _ in self?.refreshG() } ))
        alert.addAction(UIAlertAction(title: "niveau 1", style: .default, handler: {[weak self] _ in self?.level = 1
            self?.refreshG()
            self?.level = 1} ))
        
        alert.addAction(UIAlertAction(title: "niveau 2", style: .default, handler: {[weak self] _ in self?.refreshG()
            self?.level = 2  } ))
        
        alert.addAction(UIAlertAction(title: "niveau 3", style: .default, handler: {[weak self] _ in self?.refreshG()
            self?.level = 3  } ))
        
        alert.addAction(UIAlertAction(title: "Annuler", style: .cancel, handler: nil))
            
            
        viewController?.present(alert, animated: false, completion:nil)
    }
    func meldPommiers(){
        for l in 0...4 {
            for c in 0...4 {
                let val = pommiers[l][c]
                let lr = Int(arc4random()  % 4)
                let cr = Int(arc4random()  % 4)
                
                
               
                pommiers[l][c] = pommiers[lr][cr]
                pommiers[lr][cr] = val
                if (pommiers[lr][cr]  == 0 ) {
                    line  = lr
                    col = cr
                }
                if (pommiers[l][c] == 0 ) {
                    line = l
                    col = c
                    
                }
                
                
            }
        }
    }
    func refreshG(){
        //self.selectable = true
        line = 3
        col = 1
        level = 3
        pommeRouge1 = 0
        pommeVerte1 = 0
        pommeRouge2 = 0
        pommeVerte2 = 0
        


        pommiers  = oldPommiers
        oldPommiers = pommiers
        meldPommiers()
        
        
      
        
        self.mydraw()
        self.drawPommes()
        self.drawCeilleur()
         debugPrint(" END REFRESH")
        self.isUserInteractionEnabled = true
        
    }
    override func didMove(to view: SKView) {
        debugPrint("Load from skview")
        oldPommiers = pommiers
        self.mydraw()
        self.drawPommes()
        self.drawCeilleur()
        
        
        
    }
    //UPDATE
    override func update(_ currentTime: TimeInterval) {
        if self.refresh {
            refreshGame()
            self.refresh = false
        }
        //levelLabel
        let  node = (self.childNode(withName: "levelLabel"))! as! SKLabelNode
        node.text = "\(self.level)"
       // debugPrint(" TIME ")
        
       let  node2 = (self.childNode(withName: "C11"))! as! SKSpriteNode
      // node2.alpha = 0.6
        // node2.zRotation = 1
        //node2.setScale(1)
        //node2.addGlow()
       /* var light = SKLightNode();
        let _ambientColor = UIColor.red
        light.position = CGPoint(x: 0, y:0)
        //light.falloff = 10
        light.ambientColor = _ambientColor
        light.lightColor = UIColor.gray
        node2.addChild(light )*/
      /* if self.isUserInteractionEnabled{
        for l in 0...4 {
            for c in 0...4 {
                let node = (self.childNode(withName: "C\(l)\(c)"))!
                if self.pommiers[l][c] != 0 {
                    if l == self.line || c == self.col {
                        node.alpha = 1
                    }
                    else {
                        node.alpha = 0.70
                    }
                }
            }
            }
        }*/
       
        
    }
    
    func moveCeilleur (l:Int,c:Int,j1:Bool){
        
        if nbTour  == 0 { return }
        pommiers[line][col] = 0
        var  node = (self.childNode(withName: "C\(line)\(col)"))! as! SKSpriteNode
        let Texture = SKTexture(imageNamed: "cueilleur")
        let Sprite = SKSpriteNode(texture:Texture)
        Sprite.position = node.position
        Sprite.size = node.size
        Sprite.zPosition = 25
        self.addChild(Sprite)
        let nodeDest = (self.childNode(withName: "C\(l)\(c)"))! as! SKSpriteNode
    
        
        
        
        
        
        let startAction = SKAction.run {
           // self.isUserInteractionEnabled = false
            self.selectable = false
            node.alpha = 0
            self.line = l
            self.col = c
            debugPrint((" DEB MOVE"))
        }
        let finishedAction = SKAction.run {
            debugPrint((" END move Action"))
            //if (self.nbTour == 0 ) {return}
            
            nodeDest.texture = SKTexture(imageNamed:"cueilleur")
            nodeDest.alpha = 1
            Sprite.removeFromParent()
            
            
            let val = self.pommiers[l][c]
            
            if (val < 0){
                if (j1) {
                    self.pommeRouge1 = self.pommeRouge1 - val
                }else {
                    self.pommeRouge2 = self.pommeRouge2 - val
                }
            }
            else {
                if (j1){
                    self.pommeVerte1 = self.pommeVerte1 + val
                }
                else {
                    self.pommeVerte2 = self.pommeVerte2 + val
                }
            }
            self.pommiers[l][c] = 0
            
            
            let nr1 = (self.childNode(withName: "labelRouge1")) as! SKLabelNode
            nr1.text = "\(self.pommeRouge1)"
            let nv1 = (self.childNode(withName: "labelVerte1")) as! SKLabelNode
            nv1.text = "\(self.pommeVerte1)"
            let nr2 = (self.childNode(withName: "labelRouge2")) as! SKLabelNode
            nr2.text = "\(self.pommeRouge2)"
            let nv2 = (self.childNode(withName: "labelVerte2")) as! SKLabelNode
            nv2.text = "\(self.pommeVerte2)"
            
            
            
            
            //Tour adverse ou machine
            
           
            if (j1){
                
               //UI Indicator
                self.isEnable  = false
                
                var container  = UIView()
                container.frame = (self.scene?.view?.frame)!
                container.center = (self.scene?.view?.center)!
                container.backgroundColor = UIColor.gray.withAlphaComponent(0.1)
                
                
                
                
                let activityInd = UIActivityIndicatorView()
                activityInd.color = UIColor.red
                
                activityInd.center = CGPoint(x: (self.view?.bounds.midX)!, y: (self.view?.bounds.midY)!)
                activityInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
                 activityInd.color = UIColor.orange
                
               
                activityInd.startAnimating()
                //self.scene?.view?.addSubview(activityInd)
                
                self.scene?.view?.addSubview(container)
                container.addSubview(activityInd)
             
               
              
              //
                DispatchQueue.main.async{
                    let  (ln,cl ) = getGoodchoice3(verger: self.pommiers,pommeVerte1:self.pommeVerte1,pommeRouge1:self.pommeRouge1,pommeVerte2:self.pommeVerte2,pommeRouge2:self.pommeRouge2,colonneC:self.col,ligneC:self.line,profondeur:7,level:self.level)
                debugPrint(" \(self.pommeRouge2) \(self.pommeVerte2)  ")
                
                    self.moveCeilleur(l: ln, c: cl, j1: false)
                    activityInd.stopAnimating()
                    activityInd.removeFromSuperview()
                    container.removeFromSuperview()
                    self.isEnable  = true
                    debugPrint(" END Move")
                    
                    
                    

                }
                
            }
            else {
                 DispatchQueue.main.async{
                    self.isUserInteractionEnabled = true
                }
            }
            self.solVeFinJeu()
            self.selectable = true
            //A la main
          //  self.tour = !self.tour
          

           
        }
        let moveAction = SKAction.move(to: nodeDest.position, duration: 1)
        moveAction.timingMode = .easeInEaseOut
        
       let endAction = SKAction.run {
        
        }
        
        
        Sprite.run(SKAction.sequence([startAction,moveAction,finishedAction,endAction]))
        
       
        
        
    
    }
    func drawCeilleur(){
        let node = (self.childNode(withName: "C\(line)\(col)"))! as! SKSpriteNode
        node.texture = SKTexture(imageNamed:"cueilleur")
        node.alpha = 1
    }
    func drawPommes(){
        for l in 0...4 {
            for c in 0...4 {
                let val = pommiers[l][c]
                if (val != 0)
                {
                    debugPrint("\(val)")
                    let nameFile = iconePommier[ val + 5]
                    let node = (self.childNode(withName: "C\(l)\(c)"))! as! SKSpriteNode
                    node.texture = SKTexture(imageNamed: nameFile)
                }
                else {
                    let node = (self.childNode(withName: "C\(l)\(c)"))! as! SKSpriteNode
                    node.alpha = 0

                }
                
                
            }
        }
    }
    func mydraw(){
        
        for l in 0...4 {
            for c in 0...4 {
                let node = (self.childNode(withName: "C\(l)\(c)"))!
                node.alpha = 1
                node.zPosition = 20
              /*  if (l == self.line) {
                     node.alpha = 0.7
                }*/
                
            }
        }
        //labelRouge1
        var  node = (self.childNode(withName: "labelRouge1")) as! SKLabelNode
        node.text = "\(pommeRouge1)"
        node.zPosition = 20
        node = (self.childNode(withName: "labelVerte1")) as! SKLabelNode
        node.text = "\(pommeVerte1)"
        node.zPosition = 20
        node = (self.childNode(withName: "labelRouge2")) as! SKLabelNode
        node.text = "\(pommeRouge2)"
        node.zPosition = 20
        node = (self.childNode(withName: "labelVerte2")) as! SKLabelNode
        node.text = "\(pommeVerte2)"
        node.zPosition = 20
    }
    
    
    //Evt 
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        debugPrint(" Touch")
        
        if(isEnable ){
        for touch in touches {
            let location = touch.location(in: self)
            let node : SKNode = self.atPoint(location)
         /*   if node.name == "C31" {
                print("Hello")
            }*/
            if let name = node.name {
                if (name.contains("SelectLink")){
                    let url = URL(string:"https://boardgamegeek.com/boardgame/38499/22-pommes")
                    if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                    }
                    else {
                         UIApplication.shared.openURL(url!)
                    }
                }
                
                if (name.contains("selectLVL")){
                    if (self.nbTour == 0 ){
                   //     self.selectLevel()
                    }
                }
                if (name.contains("refresh")) {
                    //refreshGame()
                    self.refresh = true
                }
                else if (name.contains("C")){
                    if (!selectable) { return }
                   //
                    let  l = (Int((name.substring(1..<2))))!
                    let  c = (Int((name.substring(2..<3))))!
                    
                    if self.pommiers[l][c] != 0 {
                        if (l == line || c == col){
                             nbTour =  nbTour  + 1
                            self.isUserInteractionEnabled = false
                            self.moveCeilleur (l: l,c: c,j1:tour)
                            
                        }
                        }
                   // self.drawCeilleur()
                    
                }
            }
        }
        }

    }
    
}


