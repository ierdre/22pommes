//
//  table.swift
//  22 pommes
//
//  Created by Richard Urunuela on 12/03/2017.
//  Copyright © 2017 Richard Urunuela. All rights reserved.
//

import UIKit
import SpriteKit
class table: SKView {
       
    var viewController:ViewController?
   
    override func didMoveToWindow() {
        debugPrint("load VUE")
        let scene = plateau(fileNamed:"plateau")!
        scene.viewController = viewController
        self.ignoresSiblingOrder = true
        self.isMultipleTouchEnabled = true
        debugPrint("\(self.frame.size)")
        scene.size = CGSize(width: self.frame.size.width*2, height:self.frame.size.height*2)
        scene.scaleMode = .aspectFit
        scene.meldPommiers()

       
        self.presentScene(scene)
    }


}
