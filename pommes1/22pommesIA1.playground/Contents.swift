//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

var originalArray = [[-1, 3, 5, 1,-3],
[-5,-5,2,1,5],
[5,1,2,-1,3],
[1,0,-3,-5,-5],
[-3,5,3,-2,-1]
]


/*var originalArray = [[0, -3, 0, 1,3],
                     [0,-5,3,0,1],
                     [0,3,0,0,5],
                     [2,0,0,5,-5],
                     [3,-5,-3,0,-1]
]
*/




func recMinMAx(verger: [[Int]],v1:Int,r1:Int,v2:Int,r2:Int,xc:Int,yc:Int,tour:Bool,profondeur:Int)->Int{
    verger
    var res = 0
    var nr1 = r1
    var nr2 = r2
    var nv1 = v1
    var nv2 = v2
    
    if (v1 == 11) && (r1 == 11) {return 1}
    if (v2 == 11) && (r2 == 11) {return -1}
    if v1 >= 11 {return -1}
    if r1 >= 11 {return -1}
    if v2 > 11 {return 1}
    if r2 > 11 {return 1}
    if (profondeur == 0 ) {return res}
    
    //Exploration
    for x in 0...4 {
        if (verger[x][yc] != 0 ){
            let fruitc = verger[x][yc]
            if fruitc < 0 {
                if tour {
                    nr1 = r1 - fruitc
                }
                else {
                     nr2 = r2 - fruitc
                }
            }
            else {
                if tour {
                    nv1 = v1 + fruitc
                }
                else {
                    nv2 = v2 + fruitc
                }
            }
            
            var rverger = verger
            rverger[x][yc] = 0
            res = res + recMinMAx(verger: rverger, v1: nv1, r1: nr1, v2: nv2, r2: nr2, xc: x, yc: yc,tour: !tour,  profondeur: profondeur - 1)
        }
        
        
        
    }
    for y in 0...4 {
        if (verger[xc][y] != 0 ){
            let fruitc = verger[xc][y]
            if fruitc < 0 {
                if tour {
                    nr1 = r1 - fruitc
                }
                else {
                    nr2 = r2 - fruitc
                }
            }
            else {
                if tour {
                    nv1 = v1 + fruitc
                }
                else {
                    nv2 = v2 + fruitc
                }
            }
            
            var rverger = verger
            rverger[xc][y] = 0
            res = res + recMinMAx(verger: rverger, v1: nv1, r1: nr1, v2: nv2, r2: nr2, xc: xc, yc: y,tour: !tour,  profondeur: profondeur - 1)
        }
        
        
        
    }
    
    
    
    return res
    
}

func affRecMac (verger: [[Int]],v1:Int,r1:Int,v2:Int,r2:Int,xc:Int,yc:Int,tour:Bool,profondeur:Int){
    for x in 0...4 {
        
        if (verger[x][yc] != 0) {
            let ve2 = verger
        var res = recMinMAx(verger: ve2 , v1: v1, r1: v1, v2: v2, r2: r2, xc: x, yc: yc,tour: tour,  profondeur: profondeur)
        debugPrint("\(x) \(yc) = \(res )")
        }

    }
    for y in 0...4{
        if (verger[xc][y] != 0) {
        let ve2 = verger
        var res = recMinMAx(verger: ve2 , v1: v1, r1: r1, v2: v2, r2: r2, xc: xc, yc: yc,tour: tour,  profondeur: profondeur)
             debugPrint("\(xc) \(y) = \(res )")
        }
    }
    
    
    
}


//recMinMAx(verger: originalArray , v1: 7, r1: 7, v2: 7, r2: 7, xc: 1, yc: 3,tour: true,  profondeur: 2)
affRecMac(verger: originalArray , v1: 3, r1: 3, v2: 3, r2: 3, xc: 2, yc: 1,tour: false,  profondeur: 3)


