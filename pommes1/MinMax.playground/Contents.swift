//: Playground - noun: a place where people can play

import UIKit


var originalArray = [[-1, 3, 5, 1,-3],
                     [-5,-5,2,1,5],
                     [5,1,2,-1,3],
                     [1,0,-3,-5,-5],
                     [-3,5,3,-2,-1]
]
var pommeRouge1 = 0
var pommeVerte1 = 0
var pommeRouge2 = 0
var pommeVerte2 = 0

/*var originalArray = [[0, -3, 0, 1,3],
 [0,-5,3,0,1],
 [0,3,0,0,5],
 [2,0,0,5,-5],
 [3,-5,-3,0,-1]
 ]
 */


/*
 fonction Max : entier
 
 si profondeur = 0 OU fin du jeu alors
 renvoyer eval(etat_du_jeu)
 
 max_val <- -infini
 
 Pour tous les coups possibles
 simuler(coup_actuel)
 val <- Min(etat_du_jeu, profondeur-1)
 
 si val > max_val alors
 max_val <- val
 fin si
 
 annuler_coup(coup_actuel)
 fin pour
 
 renvoyer max_val
 fin fonction
 */

func Max (verger: [[Int]], colonneC: Int, ligneC: Int,tour:Bool,profondeur:Int,nombreCoup:Int,
             pommeRouge1: Int,
             pommerVerte1 : Int,
             pommeRouge2: Int,
             pommerVerte2:Int)->Int{
    
    
    
    if (pommeRouge1 == 11) && (pommerVerte1 == 11) {return 100-nombreCoup}
    if (pommeRouge2 == 11) && (pommeVerte2 == 11) {return -100+nombreCoup}
    if pommeRouge1 >= 11 {return -100+nombreCoup}
    if pommeVerte1 >= 11 {return -100+nombreCoup}
    if pommeRouge2 > 11 {return 100-nombreCoup}
    if pommeVerte2 > 11 {return 100-nombreCoup}
    if profondeur == 0 {return 0}
    var maxVal = Int.min
    for ligne in 0...4 {
        var pr1  = pommeRouge1
        var pv1  = pommeVerte1
        var pr2  = pommeRouge2
        var pv2  = pommeVerte2
       if verger[ligne][colonneC] != 0 {
            var  vergerSimuler = verger
            let pm = vergerSimuler[ligne][colonneC]
            if (pm>0 ) {
                pv1 = pommeVerte1 +  pm
            }
            else {
                pr1 = pommeRouge1 - pm
            }
            vergerSimuler[ligne][colonneC]  = 0
            
            let val = Min(verger: vergerSimuler,
                          colonneC: colonneC,
                          ligneC: ligne,
                          tour: true,
                          profondeur: profondeur-1 ,
                          nombreCoup:nombreCoup+1,
                          pommeRouge1: pr1,
                          pommerVerte1 : pv1,
                          pommeRouge2:pr2,
                          pommerVerte2:pv2)
            if val > maxVal  {
                maxVal = val
            }
        }
    }
    
    for colonne in 0...4 {
        var pr1  = pommeRouge1
        var pv1  = pommeVerte1
        var pr2  = pommeRouge2
        var pv2  = pommeVerte2
        if verger[ligneC][colonne] != 0 {
            var  vergerSimuler = verger
            let pm = vergerSimuler[ligneC][colonne]
            if (pm>0 ) {
                pv1 = pommeVerte1 +  pm
            }
            else {
                pr1 = pommeRouge1 - pm
            }
            vergerSimuler[ligneC][colonne]  = 0
            
            let val = Min(verger: vergerSimuler,
                          colonneC: colonne,
                          ligneC: ligneC,
                          tour: true,
                          profondeur: profondeur-1 ,
                          nombreCoup:nombreCoup+1,
                          pommeRouge1: pr1,
                          pommerVerte1 : pv1,
                          pommeRouge2:pr2,
                          pommerVerte2:pv2)
            if val > maxVal  {
                maxVal = val
            }
        }
    }
    
    
    return maxVal

}
/*
 fonction Min : entier
 
 si profondeur = 0 OU fin du jeu alors
 renvoyer eval(etat_du_jeu)
 
 min_val <- infini
 
 Pour tous les coups possibles
 simuler(coup_actuel)
 val <- Max(etat_du_jeu, profondeur-1)
 
 si val < min_val alors
 min_val <- val
 fin si
 
 annuler_coup(coup_actuel)
 fin pour
 
 renvoyer min_val
 fin fonction
 */
func Min (verger: [[Int]], colonneC: Int, ligneC: Int,tour:Bool,profondeur:Int,nombreCoup:Int,
                 pommeRouge1: Int,
                 pommerVerte1 : Int,
                 pommeRouge2: Int,
                 pommerVerte2:Int)->Int
{
    
    
    if (pommeRouge1 == 11) && (pommerVerte1 == 11) {return 100-nombreCoup}
    if (pommeRouge2 == 11) && (pommeVerte2 == 11) {return -100+nombreCoup}
    if pommeRouge1 >= 11 {return -90+nombreCoup}
    if pommeVerte1 >= 11 {return -90+nombreCoup}
    if pommeRouge2 > 11 {return 90-nombreCoup}
    if pommeVerte2 > 11 {return 90-nombreCoup}
    if profondeur == 0 {return 0}
    var minVal = Int.max
    for ligne in 0...4 {
        var pr1  = pommeRouge1
        var pv1  = pommeVerte1
        var pr2  = pommeRouge2
        var pv2  = pommeVerte2
       if verger[ligne][colonneC] != 0 {
            var  vergerSimuler = verger
            let pm = vergerSimuler[ligne][colonneC]
            if (pm>0 ) {
                pv2 = pommeVerte2 +  pm
            }
            else {
                pr2 = pommeRouge2 - pm
            }
            vergerSimuler[ligne][colonneC]  = 0
            
            let val = Max(verger: vergerSimuler,
                          colonneC: colonneC,
                          ligneC: ligne,
                          tour: true,
                          profondeur: profondeur-1 ,
                          nombreCoup:0,
                          pommeRouge1: pr1,
                          pommerVerte1 : pv1,
                          pommeRouge2:pr2,
                          pommerVerte2:pv2)
            if val < minVal  {
                minVal = val
            }
        }
    }
    
    for colonne in 0...4 {
        var pr1  = pommeRouge1
        var pv1  = pommeVerte1
        var pr2  = pommeRouge2
        var pv2  = pommeVerte2
        if verger[ligneC][colonne] != 0 {
            var  vergerSimuler = verger
            let pm = vergerSimuler[ligneC][colonne]
            if (pm>0 ) {
                pv2 = pommeVerte2 +  pm
            }
            else {
                pr2 = pommeRouge2 - pm
            }
            vergerSimuler[ligneC][colonne]  = 0
            
            let val = Max(verger: vergerSimuler,
                          colonneC: colonne,
                          ligneC: ligneC,
                          tour: true,
                          profondeur: profondeur-1 ,
                          nombreCoup:nombreCoup+1,
                          pommeRouge1: pr1,
                          pommerVerte1 : pv1,
                          pommeRouge2:pr2,
                          pommerVerte2:pv2)
            if val < minVal  {
                minVal = val
            }
        }
    }
    
    
    
    return minVal
}
/*
 fonction jouer : void
 
 max_val <- -infini
 
 Pour tous les coups possibles
 simuler(coup_actuel)
 val <- Min(etat_du_jeu, profondeur)
 
 si val > max_val alors
 max_val <- val
 meilleur_coup <- coup_actuel
 fin si
 
 annuler_coup(coup_actuel)
 fin pour
 
 jouer(meilleur_coup)
 fin fonction

 */
func jouer (verger: [[Int]], colonneC: Int, ligneC: Int,tour:Bool,profondeur:Int,
    pommeRouge1: Int,
    pommerVerte1 : Int,
    pommeRouge2: Int,
    pommerVerte2:Int)
    {
        typealias mCoup = ( colonne:Int ,ligne:Int)
        var meilleurCoup : mCoup
        var max_val = Int.min
        meilleurCoup.colonne  = -1
        meilleurCoup.ligne = -1
        let nombreCoup = 0
        
    //    let f1 = minMax
    
      //  f1(3)
    
    for ligne in 0...4 {
        var pr1  = pommeRouge1
        var pv1  = pommeVerte1
        var pr2  = pommeRouge2
        var pv2  = pommeVerte2

        
        
        if verger[ligne][colonneC] != 0 {
            var  vergerSimuler = verger
            let pm = vergerSimuler[ligne][colonneC]
            if (pm>0 ) {
                pv1 = pommeVerte1 +  pm
            }
            else {
                pr1 = pommeRouge1 - pm
            }
            vergerSimuler[ligne][colonneC]  = 0
            
            let val = Min(verger: vergerSimuler,
                       colonneC: colonneC,
                       ligneC: ligne,
                       tour: true,
                       profondeur: profondeur-1,
                       nombreCoup: nombreCoup+1,
                       pommeRouge1: pr1,
                       pommerVerte1 : pv1,
                       pommeRouge2:pr2,
                       pommerVerte2:pv2)
            if val > max_val {
                max_val = val
                meilleurCoup.colonne  = colonneC
                meilleurCoup.ligne = ligne
                
            }
            debugPrint("\(meilleurCoup) \(val) \(ligne)")
            
            
        }
        
    }
        
        for colonne in 0...4 {
            var pr1  = pommeRouge1
            var pv1  = pommeVerte1
            var pr2  = pommeRouge2
            var pv2  = pommeVerte2
            
            
            
            if verger[ligneC][colonne] != 0 {
                var  vergerSimuler = verger
                let pm = vergerSimuler[ligneC][colonne]
                if (pm>0 ) {
                    pv1 = pommeVerte1 +  pm
                }
                else {
                    pr1 = pommeRouge1 - pm
                }
                vergerSimuler[ligneC][colonne]  = 0
                
                let val = Min(verger: vergerSimuler,
                              colonneC: colonne,
                              ligneC: ligneC,
                              tour: true,
                              profondeur: profondeur-1,
                              nombreCoup:nombreCoup+1,
                              pommeRouge1: pr1,
                              pommerVerte1 : pv1,
                              pommeRouge2:pr2,
                              pommerVerte2:pv2)
                if val > max_val {
                    max_val = val
                    meilleurCoup.colonne  = colonne
                    meilleurCoup.ligne = ligneC
                    
                }
                debugPrint("\(meilleurCoup) \(val) \(colonne) ")
                
                
            }
            
        }
   
        
    
}

jouer(verger: originalArray,
          colonneC: 1,
          ligneC: 3,
          tour: true,
          profondeur: 6,
          pommeRouge1: 8,
          pommerVerte1 : 8,
          pommeRouge2:8,
          pommerVerte2:8)


