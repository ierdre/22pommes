//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"


var originalArray = [[-1, 3, 5, 1,-3],
                     [-5,-5,2,1,5],
                     [5,1,2,-1,3],
                     [1,0,-3,-5,-5],
                     [-3,5,3,-2,-1]
]



func recMinMax(verger :[[Int]],
               pommesVerte1:Int,
               pommesRouge1:Int,
               pommesVerte2:Int,
               pommesRouge2:Int,
               tour:Bool,
               ceilleurColonne:Int,
               ceilleurLigne:Int,
               profondeur:Int)->Int{
        var res = 0
    
        var pv1 = pommesVerte1
        var pv2 = pommesVerte2
        var pr1 = pommesRouge1
        var pr2 = pommesRouge2
    
    
    if (pv1 == 11) && (pr1 == 11 ) {return (1)}
    if (pv2 == 11) && (pr2 == 11 ) {return (1)}
    if pv1 > 11 {return (-1)}
    if pr1 > 11 {return (-1)}
    if pv2 > 11 {return (1)}
    if pr2 > 11 {return 1}
    
    if (profondeur == 0 ) {return (res)}
    
    for colonne in 0...4{
        let val = verger[ceilleurLigne][colonne]
        if (val != 0 ) {
            var tmpverger = verger
            tmpverger[ceilleurLigne][colonne] = 0
            if val > 0 {
                if tour  {pv1 = pommesVerte1 + val }
                else {pv2 = pommesVerte2 + val}
            }
            else {
                if tour { pr1 = pommesRouge1 - val }
                else {pr2 = pommesRouge2 - val}
            }
            
            //Enf val !=0
            res = res + recMinMax(verger: tmpverger  ,
                      pommesVerte1: pv1,
                      pommesRouge1: pr1,
                      pommesVerte2: pv2,
                      pommesRouge2: pr2,
                      tour: !tour,
                      ceilleurColonne: colonne,
                      ceilleurLigne: ceilleurLigne,
                      profondeur: profondeur-1)
        }
        
    }
    
    for ligne in 0...4{
        let val = verger[ligne][ceilleurColonne]
        if (val != 0 ) {
            var tmpverger = verger
            tmpverger[ligne][ceilleurColonne] = 0
            if val > 0 {
                if tour  {pv1 = pommesVerte1 + val }
                else {pv2 = pommesVerte2 + val}
            }
            else {
                if tour { pr1 = pommesRouge1 - val }
                else {pr2 = pommesRouge2 - val}
            }
            
            //Enf val !=0
            res = res + recMinMax(verger: tmpverger  ,
                                  pommesVerte1: pv1,
                                  pommesRouge1: pr1,
                                  pommesVerte2: pv2,
                                  pommesRouge2: pr2,
                                  tour: !tour,
                                  ceilleurColonne: ceilleurColonne,
                                  ceilleurLigne: ligne,
                                  profondeur: profondeur-1)
        }
        
    }
    
    
    
    
    return (res)
    
}
func getGoodchoice(verger :[[Int]],
                   pommesVerte1:Int,
                   pommesRouge1:Int,
                   pommesVerte2:Int,
                   pommesRouge2:Int,
                   
                   ceilleurColonne:Int,
                   ceilleurLigne:Int,
                   profondeur:Int){
    
    for colonne in 0...4{
        
        let val = verger[ceilleurLigne][colonne]
        if val != 0 { //Il ya des pommes
            var tmpVerger  = verger
            tmpVerger[ceilleurLigne][colonne] = 0
            if (val>0){
            let res = recMinMax(verger: tmpVerger  ,
                                pommesVerte1: pommesVerte1+val,
                                pommesRouge1: pommesRouge1,
                                pommesVerte2: pommesVerte2,
                                pommesRouge2: pommesRouge2,
                                tour: false,
                                ceilleurColonne: colonne,
                                ceilleurLigne: ceilleurLigne,
                                profondeur: profondeur)
                debugPrint("\(res)")
            }
            if (val<0){
                let res = recMinMax(verger: tmpVerger  ,
                                    pommesVerte1: pommesVerte1,
                                    pommesRouge1: pommesRouge1-val,
                                    pommesVerte2: pommesVerte2,
                                    pommesRouge2: pommesRouge2,
                                    tour: false,
                                    ceilleurColonne: colonne,
                                    ceilleurLigne: ceilleurLigne,
                                    profondeur: profondeur)
                debugPrint("\(res)")
            }
            
            
        }
        
    }
    for ligne in 0...4{
        
        let val = verger[ligne][ceilleurColonne]
        if val != 0 { //Il ya des pommes
            var tmpVerger  = verger
            tmpVerger[ligne][ceilleurColonne] = 0
            if (val>0){
                let res = recMinMax(verger: tmpVerger  ,
                                    pommesVerte1: pommesVerte1+val,
                                    pommesRouge1: pommesRouge1,
                                    pommesVerte2: pommesVerte2,
                                    pommesRouge2: pommesRouge2,
                                    tour: false,
                                    ceilleurColonne: ceilleurColonne,
                                    ceilleurLigne: ligne,
                                    profondeur: profondeur)
                debugPrint("\(res)")
            }
            if (val<0){
                let res = recMinMax(verger: tmpVerger  ,
                                    pommesVerte1: pommesVerte1,
                                    pommesRouge1: pommesRouge1-val,
                                    pommesVerte2: pommesVerte2,
                                    pommesRouge2: pommesRouge2,
                                    tour: false,
                                    ceilleurColonne: ceilleurColonne,
                                    ceilleurLigne: ligne,
                                    profondeur: profondeur)
                debugPrint("\(res)")
            }
            
            
        }
        
    }
    
    
    
    
}

getGoodchoice(verger :originalArray,
              pommesVerte1:5,
              pommesRouge1:5,
              pommesVerte2:5,
              pommesRouge2:5,
              
              ceilleurColonne:1,
              ceilleurLigne:3,
              profondeur:1)
var e = 5
Int((1.0/3) * 100)